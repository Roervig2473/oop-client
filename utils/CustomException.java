package pw.icake.OOP.utils;

import pw.icake.OOP.OOP;

public class CustomException extends Exception
{

	public CustomException(String error)
	{
		
		super(error);
		
	}
	
	public void printError()
	{
		
		OOP.getInstance().printError(this.getMessage());
		
	}
	
}