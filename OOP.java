package pw.icake.OOP;

import net.minecraft.src.*;
import pw.icake.OOP.hacks.HackManager;

public class OOP
{

	private static OOP instance;
	private HackManager hackManager;
	
	public OOP()
	{
		
		this.setInstance();
		this.setHackManager();
		
	}
	
	/**
	 * 
	 * @param toPrint
	 */
	public void println(String toPrint)
	{
		
		System.out.println("[" + this.getName() + " : " + this.getAuthor() + "]: " + toPrint);
		
	}
	
	/**
	 * 
	 * @param type
	 * @param toPrint
	 */
	public void println(String type, String toPrint)
	{
		
		System.out.println("[" + this.getName() + " : " + this.getAuthor() + " (" + type + ")]: " + toPrint);
		
	}
	
	/**
	 * 
	 * @param toPrint
	 */
	public void printError(String toPrint)
	{
		
		System.err.println("[" + this.getName() + " | " + this.getAuthor() + "]: " + toPrint);
		
	}
	
	private void setInstance()
	{
		
		instance = this;
		
	}
	
	/**
	 * 
	 * @return The class instance
	 */
	public static OOP getInstance()
	{
		
		return instance;
		
	}
	
	public Minecraft getMinecraft()
	{
		
		return Minecraft.getMinecraft();
		
	}
	
	/**
	 * 
	 * @return hackmanager
	 */
	public HackManager getHackManager()
	{
		
		return this.hackManager;
		
	}
	
	protected void setHackManager()
	{
		
		this.hackManager = new HackManager();
		
	}
	
	/**
	 * 
	 * @return Class Name
	 */
	public String getName()
	{
		
		return this.getClass().getSimpleName();
		
	}
	
	/**
	 * 
	 * @return author
	 */
	public String getAuthor()
	{
		
		return "Roervig2473";
		
	}
	
	public float getVersion()
	{
		
		return 0.1F;
		
	}
	
}