package pw.icake.OOP.hacks;

import pw.icake.OOP.utils.CustomException;

public class HackExistsException extends CustomException
{

	public HackExistsException(Hack hack)
	{
		
		super("Attempted to add hack " + hack.toString() + ", but it's already added!");
		this.printError();
		
	}
	
}