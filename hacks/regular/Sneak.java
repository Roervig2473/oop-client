package pw.icake.OOP.hacks.regular;

import org.lwjgl.input.Keyboard;

import pw.icake.OOP.OOP;
import pw.icake.OOP.hacks.Hack;

public class Sneak extends Hack
{

	public Sneak()
	{
	
		super("Sneak", Keyboard.KEY_C, "Default");

	}
	
	public void onEnable()
	{
		
		this.println("Stuff");
		
	}
	
	@Override
	public void onSafeTick()
	{
		
		OOP.getInstance().getMinecraft().thePlayer.setSneaking(true);
		
	}

}