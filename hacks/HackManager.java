package pw.icake.OOP.hacks;

import java.util.ArrayList;

import pw.icake.OOP.OOP;
import pw.icake.OOP.hacks.regular.Sneak;

public class HackManager
{

	private ArrayList<Hack> hacks;
	
	public HackManager()
	{
		
		this.setHacks();
		this.silentAddHack(new Sneak());
		
	}
	
	public void println(String toPrint)
	{
		
		OOP.getInstance().println("Hack Manager", toPrint);
		
	}
	
	public void silentAddHack(Hack hack)
	{
		
		try
		{
		
			this.addHack(hack);
		
		}
		catch (HackExistsException e)
		{
		
		}
		
	}
	
	public void addHack(Hack hack) throws HackExistsException
	{
		
		this.println("Adding: " + hack.getName() + ", to the hack list");
		
		if(this.getHacks().contains(hack))
		{
			
			throw new HackExistsException(hack);
			
		}
		
		this.getHacks().add(hack);
		
	}
	
	public ArrayList<Hack> getHacks()
	{
		
		return this.hacks;
		
	}
	
	protected void setHacks()
	{
		
		this.hacks = new ArrayList<Hack>();
		
	}
	
}