package pw.icake.OOP.hacks;

import pw.icake.OOP.OOP;

public abstract class Hack
{

	private String name;
	private String desc;
	private int key;
	private String permission;
	private boolean state;
	private char colorCode;
	
	/**
	 * 
	 * @param name
	 * @param key
	 * @param permission
	 */
	public Hack(String name, int key, String permission)
	{
		
		this.setName(name);
		this.setKey(key);
		this.setPermission(permission);
		
	}
	
	/**
	 * 
	 * @param name
	 * @param desc
	 * @param key
	 * @param permission
	 * @param state
	 */
	public Hack(String name, String desc, int key, String permission, boolean state)
	{
		
		this.setName(name);
		this.setDescription(desc);
		this.setKey(key);
		this.setPermission(permission);
		this.setState(state);
		
	}
	
	/**
	 * 
	 * @param name
	 * @param desc
	 * @param key
	 * @param state
	 */
	public Hack(String name, String desc, int key, boolean state)
	{
		
		this.setName(name);
		this.setDescription(desc);
		this.setKey(key);
		this.setState(state);
		this.setPermission();
		
	}
	
	/**
	 * 
	 * @param name
	 * @param key
	 * @param state
	 */
	public Hack(String name, int key, boolean state)
	{
		
		this.setName(name);
		this.setKey(key);
		this.setState(state);
		this.setPermission();
		
	}
	
	public void onToggle()
	{
		
	}
	
	public void onEnable()
	{
		
	}
	
	public void onDisable()
	{
		
	}
	
	public void onTick()
	{
		
	}
	
	public void onSafeTick()
	{
		
	}
	

	public void onKeyPressed()
	{
		
		this.onToggle();
		this.setState(this.isEnabled() ? false : true);
		
		if(this.isEnabled())
		{
			
			this.onEnable();
			
		}
		else
		{
			
			this.onDisable();
			
		}
		
	}
	
	/**
	 * 
	 * @return hack name
	 */
	public String getName()
	{
		
		return this.name;
		
	}
	
	/**
	 * 
	 * @param name
	 */
	public void setName(String name)
	{
		
		this.name = name;
		
	}
	
	/**
	 * 
	 * @return hack description
	 */
	public String getDescription()
	{
		
		return this.desc;
		
	}
	
	/**
	 * 
	 * @param desc
	 */
	public void setDescription(String desc)
	{
		
		this.desc = desc;
		
	}
	
	/**
	 * 
	 * @return keyboard shortcut
	 */
	public int getKey()
	{
		
		return this.key;
		
	}
	
	/**
	 * 
	 * @param key
	 */
	public void setKey(int key)
	{
		
		this.key = key;
		
	}
	
	/**
	 * 
	 * @return permission
	 */
	public String getPermission()
	{
		
		return this.permission;
		
	}

	protected void setPermission(String permission)
	{
		
		this.permission = permission;
		
	}
	
	protected void setPermission()
	{
		
		this.permission = "Default";
		
	}
	
	/**
	 * 
	 * @return hack state
	 */
	public boolean isEnabled()
	{
		
		return this.state;
		
	}
	
	/**
	 * 
	 * @param state
	 */
	protected void setState(boolean state)
	{
		
		this.state = state;
		
	}
	
	/**
	 * 
	 * @return hack colorcode
	 */
	public char getColorCode()
	{
		
		return this.colorCode;
		
	}
	
	/**
	 * 
	 * @param colorCode
	 */
	public void setColorCode(char colorCode)
	{
	
		this.colorCode = colorCode;
		
	}
	

	/**
	 * 
	 * @return class to a string
	 */
	@Override
	public String toString()
	{
		
		if(this.getDescription() != null)
		{
			
			return "( " + this.getName() + " :|: " + this.getDescription() + " )";
			
		}
		
		return "( " + this.getName() + " )";
		
	}
	
	/**
	 * 
	 * @param toPrint
	 */
	public void println(String toPrint)
	{
		
		OOP.getInstance().println("Hack: " + this.getName(), toPrint);
		
	}
	
}