package pw.icake.OOP.gui;

import pw.icake.OOP.OOP;
import pw.icake.OOP.hacks.Hack;
import net.minecraft.src.*;

public class Ingame
{
	
	public Ingame()
	{
		
	}
	
	public void renderGameOverlay(float par1, boolean par2, int par3, int par4)
	{

		OOP.getInstance().getMinecraft().fontRenderer.drawStringWithShadow("�2[�a" + OOP.getInstance().getName() + " �f: �2[�a" + OOP.getInstance().getVersion() + "�2]]", 4, 4, 0xFFFFFF);
		
		int counter = 16;
		
		for(Hack hack : OOP.getInstance().getHackManager().getHacks())
		{
			
			hack.onTick();
			
			if(hack.isEnabled())
			{
				
				hack.onSafeTick();
				OOP.getInstance().getMinecraft().fontRenderer.drawStringWithShadow(hack.getName(), 4, counter, 0xFFFFFF);
				counter += 12;
				
			}
			
		}
		
	}
	
}